using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    
    private static InputManager instance;

    public static InputManager Instance
    {
        get => instance;
    }

    [SerializeField] private float _mouseHorizontal;
    [SerializeField] private float _mouseVertical;

    public float MouseHorizontal
    {
        get => _mouseHorizontal;
    }
    
    public float MouseVertical
    {
        get => _mouseVertical;
    }

    [SerializeField] private float _keyboardHorizontal;
    [SerializeField] private float _keyboardVertical;
    
    public float KeyboardHorizontal
    {
        get => _keyboardHorizontal;
    }
    
    public float KeyboardVertical
    {
        get => _keyboardVertical;
    }
    
    [Header("Jump Input")]
    [SerializeField] private bool isJump;
    public bool IsJump
    {
        get => isJump;
    }
    [SerializeField] private KeyCode jumpKey = KeyCode.Space;
    
    private void Awake()
    {
        if (InputManager.instance != null)
        {
            Debug.LogError("Only 1 Input Manager exist!");
            return;
        }
        InputManager.instance = this;
    }

    private void Update()
    {
        this._mouseHorizontal = Input.GetAxisRaw("Mouse X");
        this._mouseVertical = Input.GetAxisRaw("Mouse Y");
        this._keyboardVertical = Input.GetAxisRaw("Vertical");
        this._keyboardHorizontal = Input.GetAxisRaw("Horizontal");
        this.isJump = Input.GetKey(jumpKey);
    }
}
