using System.Collections;
using System.Collections.Generic;
using Unity.Mathematics;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class InteractionObject : MonoBehaviour
{
    private Transform entity;
    [SerializeField] private bool isOpen;
    [SerializeField] private Quaternion baseOrientation;
    [SerializeField] private Quaternion stateOrientation;
    [SerializeField] private float speed = 1.0f;
    void Start() {
        entity = GetComponent<Transform>();
        isOpen = false;
        baseOrientation = entity.rotation;
    }

    void FixedUpdate() {
        if (isOpen)
            Close();
        else
            Open();
    }

    public void SwitchDoor() {
        isOpen = !isOpen;
    }

    void Open() {
        if (entity.rotation != baseOrientation) {
            entity.rotation = Quaternion.Lerp(entity.rotation, baseOrientation, 
                Time.fixedDeltaTime * speed);
        }
    }

    void Close() {
        if (entity.rotation != stateOrientation) {
            entity.rotation = Quaternion.Lerp(entity.rotation, stateOrientation, 
                Time.fixedDeltaTime * speed);
        }
    }
}

