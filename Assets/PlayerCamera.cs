using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{
    [Header("Setting")]
    [SerializeField] private float sensivityX;
    [SerializeField] private float sensivityY;

    [Header("View Only,Do not change")]
    [SerializeField] private Transform cameraOrientation;
    [SerializeField] private Transform cameraPosition;
    [SerializeField] private float xRotation;
    [SerializeField] private float yRotation;
    
    void Start()
    {
        cameraOrientation = GameObject.Find("Orientation").transform;
        cameraPosition = GameObject.Find("Camera First Position").transform;
    }   

    private void Reset()
    {
        sensivityX = 200;
        sensivityY = 200;
        cameraOrientation = GameObject.Find("Orientation").transform;
        cameraPosition = GameObject.Find("Camera First Position").transform;
    }

    void Update()
    {
        UpdatePosition();
        UpdateOrientation();
    }

    void UpdatePosition()
    {
        transform.position = cameraPosition.position;
    }

    void UpdateOrientation()
    {
        yRotation += InputManager.Instance.MouseHorizontal * Time.deltaTime *
                     sensivityX;
        xRotation -= InputManager.Instance.MouseVertical * Time.deltaTime *
                     sensivityX;
        xRotation = Mathf.Clamp(xRotation, -90, 90f);
        transform.rotation = Quaternion.Euler(xRotation, yRotation, 0);
        cameraOrientation.rotation = Quaternion.Euler(0, yRotation, 0);
    }
}
