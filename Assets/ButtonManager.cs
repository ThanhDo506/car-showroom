using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    [SerializeField] private List<Button> _buttons;
    [SerializeField] private bool isDisplay = true;

    public void OnClick()
    {
        if (isDisplay)
        {
            HideAll();
        }
        else
        {
            DisplayAll();
        }
    }

    private void DisplayAll()
    {
        isDisplay = true;
        foreach (var button in _buttons)
        {
            button.gameObject.SetActive(true);
        }
    }

    private void HideAll()
    {
        isDisplay = false;
        foreach (var button in _buttons)
        {
            button.gameObject.SetActive(false);
        }
    }
}
