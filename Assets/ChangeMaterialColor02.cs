using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public struct CarMaterials
{
    public Material material;
    public Color color;
}

public class ChangeMaterialColor02 : MonoBehaviour
{

    [SerializeField]
    private CarMaterials[] _carMaterials;

    public void OnClick()
    {
        foreach (var h in _carMaterials)
        {
            h.material.SetColor("_BaseColor", h.color);
        }
    }
}

