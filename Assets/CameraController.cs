using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private GameObject cam1;
    [SerializeField] private GameObject cam2;
    private bool onCam1 = true;
    private void Start()
    {
        cam1.SetActive(true);
        cam2.SetActive(false);
        onCam1 = true;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F5))
        {
            if (onCam1)
            {
                cam1.SetActive(false);
                cam2.SetActive(true);
                onCam1 = false;
            }
            else
            {
                cam1.SetActive(true);
                cam2.SetActive(false);
                onCam1 = true;
            }
        }
    }
}
