using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowColorPicker : MonoBehaviour
{
    [SerializeField] private Transform button;
    [SerializeField] private bool isDisplay;
    void Start()
    {
        isDisplay = false;
        button.gameObject.SetActive(isDisplay);
    }

    public void OnClick()
    {
        if (isDisplay)
        {
            isDisplay = false;
        }
        else
        {
            isDisplay = true;
        }
        button.gameObject.SetActive(isDisplay);
    }
    
}
