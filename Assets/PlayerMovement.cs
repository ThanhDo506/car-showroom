using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private float _speed = 5.0f;
    [Range(1.0f, 5.0f)] [SerializeField] private float acceration = 5.0f;
    [SerializeField] private Vector3 moveDirection;
    [SerializeField] private float groundDrag;

    [Header("Check ground")]
    [SerializeField]
    private float playerHeight;

    [SerializeField] private float heightOffset = 0.2f;
    [SerializeField] private LayerMask groundLayer;
    [SerializeField] bool isGrounded;

    [Header("Jumping")] 
    [SerializeField] private float jumpForce;
    [SerializeField] private float jumpCooldown;
    [SerializeField] private float airMultiplier;
    [SerializeField] private bool canJump;
    

    private Transform orientation;
    private Rigidbody _rigidbody;
    void Start()
    {
        _rigidbody = GetComponentInParent<Rigidbody>();
        _rigidbody.freezeRotation = true;
        orientation = GameObject.Find("Orientation").transform;
    }

    private void Update()
    {
        this.CheckGround();
        this.SpeedController();
        this.DragHandler();
        
        if (InputManager.Instance.IsJump)
        {
            this.Jump();
        }
    }

    void FixedUpdate()
    {
        this.Move();
    }

    private void Reset()
    {
        _speed = 7.0f;
        groundDrag = 0.5f;
        playerHeight = 2.0f;
    }

    private void Move()
    {
        moveDirection = orientation.forward * InputManager.Instance.KeyboardVertical +
                        orientation.right * InputManager.Instance.KeyboardHorizontal;
        _rigidbody.AddForce(moveDirection.normalized * _speed * acceration * 
                            (isGrounded ? 1 : airMultiplier), ForceMode.Force);
    }

    private void CheckGround()
    {
        isGrounded = Physics.Raycast(transform.parent.transform.position, Vector3.down
            , playerHeight * 0.5f + heightOffset, groundLayer);
    }

    private void DragHandler()
    {
        if (isGrounded)
            _rigidbody.drag = groundDrag;
        else
            _rigidbody.drag = 0;
    }

    private Vector3 flatVel = new Vector3();
    private Vector3 limitedVel = new Vector3();
    private Vector3 adjustedVelocity;
    private void SpeedController()
    {
        flatVel = _rigidbody.velocity;
        flatVel.y = 0;
        if (flatVel.magnitude > _speed)
        {
            limitedVel = flatVel.normalized * _speed;
            adjustedVelocity = limitedVel;
            adjustedVelocity.y = _rigidbody.velocity.y;
            _rigidbody.velocity = adjustedVelocity;
        }
    }

    private void Jump()
    {
        if (!canJump || !isGrounded)
            return;
        canJump = false;
        _rigidbody.velocity = new Vector3(_rigidbody.velocity.x, 0, _rigidbody.velocity.z);
        _rigidbody.AddForce(transform.parent.transform.up * jumpForce, ForceMode.Impulse);
        Invoke(nameof(ResetJump), jumpCooldown);
    }

    private void ResetJump()
    {
        canJump = true;
    }
}
