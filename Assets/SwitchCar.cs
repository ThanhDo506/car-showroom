using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SwitchCar : MonoBehaviour
{
    [SerializeField] private List<Transform> cars;
    private int i = 0;

    private void Start() {
        for (int j = 0; j < cars.Count(); j++) {
            cars[j].gameObject.SetActive(false);
        }
        cars[i].gameObject.SetActive(true);
    }

    public void Switch() {
        if (i < cars.Count() - 1) {
            cars[i].gameObject.SetActive(false);
            i++;
            cars[i].gameObject.SetActive(true);
        } else {
            cars[i].gameObject.SetActive(false);
            i = 0;
            cars[i].gameObject.SetActive(true);
        }
    }
}
