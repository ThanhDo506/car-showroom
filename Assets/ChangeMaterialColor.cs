using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterialColor : MonoBehaviour
{
    [SerializeField] private Material _material;
    [SerializeField] private Color _color;

    private void Start()
    {
        // _material.shader = Shader.Find("Universal Render Pipeline/Lit");
    }

    public void OnClick()
    {
        _material.SetColor("_BaseColor", _color);
    }
}
