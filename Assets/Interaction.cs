using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interaction : MonoBehaviour
{
    [SerializeField] private Transform cameraTransform;
    [SerializeField] private LayerMask interactionLayer;
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
        {
            RaycastHit raycastHit;
            if (Physics.Raycast(cameraTransform.position, cameraTransform.forward
                    , out raycastHit, 10.0f, interactionLayer))
            {
                InteractionObject io
                        = raycastHit.transform.GetComponent<InteractionObject>();
                io.SwitchDoor();
            }
        }
    }
}

